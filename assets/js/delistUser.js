let params = new URLSearchParams(window.location.search)
console.log(params.get('courseId'));
console.log(params.get('userId'));

let token = localStorage.getItem("token");
let courseId = params.get('courseId');
let userId = params.get('userId');

fetch('https://calm-ravine-25596.herokuapp.com/api/users/delistAdmin', {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			userId: userId,
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if (data === true) {

			alert("Successfully removed student.");
			window.location.replace(`./course.html?courseId=${courseId}`);

		} else {

			alert("Something went wrong.");
			window.location.replace(`./course.html?courseId=${courseId}`);


		}

	})