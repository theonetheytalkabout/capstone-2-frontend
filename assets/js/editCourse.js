let params = new URLSearchParams(window.location.search)
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem("token");

console.log(token)

let formSubmit = document.querySelector("#editCourse");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	if ((courseName.length >= 1) && (courseDescription.length >= 1) && coursePrice.length >= 1) {

	fetch('https://calm-ravine-25596.herokuapp.com/api/courses', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId,
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if (data === true) {

			alert("Course successfully updated!");
			window.location.replace("./courses.html");

		} else {

			alert("Something went wrong.")

		}

	})} else {


		alert("No fields can be blank.")

	}

})

/*formSubmit.addEventListener("submit",  (e) => {

	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector('#coursePrice').value;

	// console.log(`courseName: ${courseName}`);
	// console.log(`courseId: ${courseId}`);

	e.preventDefault();

	fetch(`https://calm-ravine-25596.herokuapp.com/api/courses`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId,
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data == true){
			alert("Course successfully updated!");
			window.location.replace("./courses.html");
		}else {

			alert("Something went wrong.");
		}
	})
})*/

fetch(`https://calm-ravine-25596.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);

	formSubmit.innerHTML =
	`
	<form id="editCourse">	
			<div class="form-row">
				<div class="form-group col-md-9">
					<input type="text" id="courseName" class="form-control" placeholder="Course name" value="${data.name}">
				</div>

				<div class="form-group col-md-3">
					<input type="number" id="coursePrice" class="form-control" placeholder="Price" value="${data.price}">
				</div>
			</div>

			<textarea type="inputbox" id="courseDescription" class="form-control" placeholder="Course description" value="">${data.description}</textarea>
			<!-- end form row -->

			<button type="submit" class="btn btn-block btn-dark my-3"> Submit </button>
		</form>
	`

})