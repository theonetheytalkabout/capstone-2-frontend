let params = new URLSearchParams(window.location.search)
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let token = localStorage.getItem("token");
let courseId = params.get('courseId');

fetch('https://calm-ravine-25596.herokuapp.com/api/users/delist', {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization' : `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if (data === true) {

			alert("Successfully removed student.");
			window.location.replace("./profile.html");

		} else {

			alert("Something went wrong.");
			window.location.replace("./profile.html");


		}

	})