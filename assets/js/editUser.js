let params = new URLSearchParams(window.location.search)

console.log(params.get('userId'));

let userId = params.get('userId');

let token = localStorage.getItem("token");

let formSubmit = document.querySelector("#editProfile");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#email").value;
	let mobileNo = document.querySelector("#mobileNo").value;

	if ( firstName.length >= 1 && lastName.length >= 1 && email.length >= 1 && mobileNo.length == 11 ) {

		fetch('https://calm-ravine-25596.herokuapp.com/api/users/edit', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})

		})
		.then(res => res.json())
		.then(data => {

			if ( data == true ) {

				localStorage.setItem('firstName', firstName);
				alert('User successfully updated!');
				window.location.replace('./profile.html');

			} else {

				alert('Something went wrong.');

			}

		})

	} else if ( firstName.length !== "" || lastName.length !== "" || email.length !== "" ) {

		alert('Fields cannot be empty.');

	} else if ( mobileNo !== 11 ) {

		alert('Mobile number must be 11 digits.');

	} else {

		alert('Something went wrong.');

	}

})

fetch(`https://calm-ravine-25596.herokuapp.com/api/users/${userId}`)
.then(res => res.json())
.then(user => {

	formSubmit.innerHTML =

	`
	<!-- Start column -->
	<div class="form-group col-md-6">
		<input type="text" id="firstName" class="form-control" placeholder="First name" value="${user.firstName}">
	</div>
	<!-- End column -->
	<!-- Start column -->
	<div class="form-group col-md-6">
		<input type="text" id="lastName" class="form-control" placeholder="Last name" value="${user.lastName}">
	</div>
	<!-- End column -->
	<!-- Start column -->
	<div class="form-group col-md-6">
		<input type="email" id="email" class="form-control" placeholder="Email" value="${user.email}">
	</div>
	<!-- End column -->
	<!-- Start column -->
	<div class="form-group col-md-6">
		<input type="number" id="mobileNo" class="form-control" placeholder="Mobile number" value="${user.mobileNo}">
	</div>
	<!-- End column -->
	<button type="submit" class="btn btn-block btn-danger my-3">Submit</button>
	`

})