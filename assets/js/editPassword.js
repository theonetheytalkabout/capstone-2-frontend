let params = new URLSearchParams(window.location.search)
console.log(params.get('userId'));

let userId = params.get('userId');

let token = localStorage.getItem("token");

let formSubmit = document.querySelector("#editPassword");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let userrId = userId
	let password1 = document.querySelector("#password1").value;
	let password = document.querySelector("#password").value;
	let passwordConfirm = document.querySelector("#passwordConfirm").value;

	if ( password == passwordConfirm ) {

		fetch('https://calm-ravine-25596.herokuapp.com/api/users/updatePassword', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			body: JSON.stringify({
				password1: password1,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if ( data == true ) {

				alert("Changed password successfully.")
				window.location.replace("./profile.html")

			} else {

				alert("Old password is incorrect.")

			}

		})

	} else {

		alert("Verified password does not match.")

	}

})