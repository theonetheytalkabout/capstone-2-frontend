let adminUserr = localStorage.getItem("isAdmin");
console.log(adminUserr);

let firstName = localStorage.getItem("firstName");
console.log(firstName);
let helloBtn = document.querySelector("#helloBtn");

let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileBtn = document.querySelector("#profileBtn");

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {

	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link text-dark"><span id="shortcuts-dark">  Log In </span></a>
		</li>
	`

	registerBtn.innerHTML =
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link"><span id="shortcuts-dark"> Register </span></a>
		</li>
	`

} else {

	if (adminUserr == "false" || !adminUserr) {

	helloBtn.innerHTML = 	
	`
		<li class="nav-item">
			<span class="nav-link" id="helloButton">Hello, ${firstName}</span></a>
		</li>
	`

	profileBtn.innerHTML = 	
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"><span id="shortcuts-dark"> Profile </span></a>
		</li>
	`	
	}

	else if (adminUserr == "true" || adminUserr) { profileBtn.innerHTML = null}

	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"><span id="shortcuts-dark"> Log Out </span> </a>
		</li>
	`

}