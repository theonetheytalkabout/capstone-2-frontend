let params = new URLSearchParams(window.location.search)
console.log(params.has('courseId'));
console.log(params.get('courseId'));

console.log(params.has('courseId'));

console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem("token");

console.log(token)

fetch(`https://calm-ravine-25596.herokuapp.com/api/courses/${courseId}/hard`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	console.log(data);

	if(data === true){

			alert("Course deleted.");
			window.location.replace("./courses.html");

		} else {

			alert("Something went wrong.")

		}

})