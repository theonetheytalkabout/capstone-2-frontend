let adminUser = localStorage.getItem("isAdmin");
let currentId = localStorage.getItem("id");
console.log(currentId);
console.log(adminUser);
let cardFooter;
let modalButton = document.querySelector("#adminButton");
let password = localStorage.getItem("password");
console.log(password);

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML =
	`
	<div class="col-md-2 offset-md-5">
		<a href="./addCourse.html" class="btn btn-block btn-light">Add Course</a>
	</div>
	`

}


if ((adminUser == "true")) {
	fetch('https://calm-ravine-25596.herokuapp.com/api/courses/inactive')
	.then(res => res.json())
	.then(data => {

		console.log(data);

		let courseData;

		if(data.length < 1) {

			courseData = "No courses available"

		} else {


			courseData = data.map(course => {

				let active = course.isActive;
						console.log(active);

				if(active == "false" || !active) {

							cardFooter =
							`
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">Edit
							</a>
							<a href="./enableCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block deleteButton">Enable Course
							</a>
							`

				} else {

							cardFooter =
							`
							<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">View Enrollees
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">Edit
							</a>
							<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">Disable Course
							</a>
							`
						}

				return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body" id="coursePageBody">
							<h5 class="card-title">
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`
				)

			}).join("");

		}
		document.querySelector("#coursesContainer").innerHTML = courseData
	})

} if (adminUser == "false" || adminUser == null) {

	fetch('https://calm-ravine-25596.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		console.log(data);

		let courseData;

		if (data.length < 1) {

			courseData = "No courses available"

		} else {

			courseData = data.map(course => {

				let active = course.isActive;
				//console.log(course.enrollees);

				var found = false;
		        for(var i = 0; i < course.enrollees.length; i++) {
		            if (course.enrollees[i].userId == currentId) {
		                found = true;
		                break;
		            }
		            else {
		                found = false;
		            }
		        }

		        // console.log(found)

		        if ( found == false ) {

		        	cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-dark text-white btn-block editButton">Select Course
					</a>
					`

		        } else {

		        	cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-dark text-white btn-block editButton disabled">You are already enrolled in this course.
										</a>
					`

		        }
				
				return (
					
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body" id="coursePageBodyUser">
							<h5 class="card-title">
								${course.name}
							</h5>
							<p class="card-text text-left" id="courseDescriptionC">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱ ${course.price}
							</p>
							<p class="card-text text-right">
								No. of current enrollees: ${course.enrollees.length}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`

			)

			}).join("")

		}

		document.querySelector("#coursesContainer").innerHTML = courseData

	})


}





