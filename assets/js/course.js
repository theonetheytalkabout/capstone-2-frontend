// "window.location.search" returns the query string part of the URL
console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search)

// get isAdmin
let adminUser = localStorage.getItem("isAdmin");
// console.log(adminUser);

let currentId = localStorage.getItem("id")
console.log(currentId)

// The "has" method checks if the "courseId" key exists in the URL query string
// The method returns true if the key exists
// console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
// console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem("token");
// console.log(token)

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer")
let enrolleesContainer = document.querySelector("#enrolleesContainer");
let enrolleesHeader = document.querySelector("#enrolleesHeader");

fetch('https://calm-ravine-25596.herokuapp.com/api/courses/alreadyEnrolled', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		courseId: courseId,
		userId: currentId
	})

})
.then(res => res.json())
.then(found => {

	console.log(found)

	fetch(`https://calm-ravine-25596.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;

		let enrollees = data.enrollees;
		let active = data.isActive;

		if ( adminUser == "true" ) {

			if ( enrollees.length == 0 ) {

				enrolleesHeader.innerHTML =
				`
				<div class="container bg-dark text-white" id="enrolleesHeader"><h1>There are no enrollees</h1></div>
				`

			} else {

				enrolleesHeader.innerHTML =
				`
				<div class="container bg-dark text-white" id="enrolleesHeader"><h1>enrollees</h1><p>Number of enrollees: ${enrollees.length}</p></div>
				`

			}

			if ( active == false ) {

				enrollContainer.innerHTML =

				`
				<a href="./editCourse.html?courseId=${data._id}" value="${data._id}" class="btn btn-primary text-white btn-block editButton">Edit</a>
				<a href="./enableCourse.html?courseId=${data._id}" value="${data._id}" class="btn btn-success text-white btn-block deleteButton">Enable Course</a>
				`

			} else {

				enrollContainer.innerHTML =

				`
				<a href="./editCourse.html?courseId=${data._id}" value="${data._id}" class="btn btn-primary text-white btn-block editButton">Edit</a>
				<a href="./deleteCourse.html?courseId=${data._id}" value="${data._id}" class="btn btn-danger text-white btn-block deleteButton">Disable Course</a>
				<a href="./hardDeleteCourse.html?courseId=${data._id}" value="${data._id}" onclick="return confirm('If you click on yes the course will be deleted permanently and you will be unable to retrieve the data. Do you want to continue?')" class="btn btn-dark text-white btn-block deleteButton">Delete Course Permanently</a>
				`

			}

			if ( enrollees.length < 1 ) {

				enrolleesContainer.innerHTML = null

			} else {

				enrollees.map(enrollee => 

					fetch(`https://calm-ravine-25596.herokuapp.com/api/users/${enrollee.userId}`)
					.then(res => res.json())
					.then(specificEnrollee => {

						enrolleesContainer.innerHTML +=

						`
						<div class="card col-md-12">
							
								<div class="card-body row">
									<p class="card-title col-md-3 col-sm-3">
										${specificEnrollee.lastName}, ${specificEnrollee.firstName}
									</p>
									<p class="card-text text-center col-md-3 col-sm-3">
										${specificEnrollee.email}
									</p>
									<p class="card-text text-center col-md-3 col-sm-3">
										${specificEnrollee.mobileNo}
									</p>
									<p class="card-text text-center col-md-3 col-sm-3" id="delistContainer">
										<a href="./delistUser.html?courseId=${data._id}&userId=${specificEnrollee._id}" value="${data._id}" class="btn btn-danger text-white btn-block editButton">Delist</a>
									</p>
								</div>										
							
						</div>
						`

					})

					).join("")

			}

		} else {

			if ( found == false ) {

				enrollContainer.innerHTML =
				`
				<button id="enrollButton" class="btn btn-block btn-dark">Enroll</button>
				`

			} else {

				enrollContainer.innerHTML =
				`
				<button id="enrollButton" class="btn btn-block btn-dark" disabled>You are already enrolled in this course.</button>
				`
			}

			

			enrolleesContainer.innerHTML = null;

		}

		document.querySelector("#enrollButton").addEventListener("click", () => {

			fetch('https://calm-ravine-25596.herokuapp.com/api/users/enroll', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})

				})
				.then(res => res.json())
				.then(datum => {

					if (datum == true) {

						alert("Thank you for enrolling! See you in class!");
						window.location.replace("./courses.html");

					} else {

						alert("You must log in first.");
						window.location.replace("./login.html");

					}

				})

		})

	})

})