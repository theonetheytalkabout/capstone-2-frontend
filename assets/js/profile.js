let token = localStorage.getItem("token");
console.log(token);

let profile = document.querySelector("#profileContainer");
let courses = document.querySelector("#coursesContainer");
let containerHeader = document.querySelector("#containerHeader");

/*let enrolledCourses = new Array();
let datedCourses = new Array();*/

if( !token || token === null ) {
	alert("Please log in.");
	window.location.replace("./login.html");
} else {

	fetch('https://calm-ravine-25596.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		//console.log(data.enrollments.length);
		//console.log(data)
		let enrollments = data.enrollments;

		console.log(enrollments);


		profile.innerHTML = `
		<div class="col-md-12">
			<section class=" my-5">
				<h3 class="text-center">${data.firstName} ${data.lastName}</h3>
				<h3 class="text-center">${data.email}</h3>
				<h3 class="text-center">${data.mobileNo}</h3>
				
			</section>
		</div>
		<a href="./editUser.html?userId=${data._id}"  class="btn btn-dark text-white btn-block editButton">Edit Profile
							</a>
		<a href="./editPassword.html?userId=${data._id}"  class="btn btn-dark text-white btn-block editButton">Change Password
							</a>
		`

		containerHeader.innerHTML =

		`
		<div class="container text-white bg-dark" id="enrolleesHeader"><h4>Currently enrolled classes:</h4></div>
		`

		if (data.enrollments.length < 1) {

			courses.innerHTML =

				`
				<table class="table">
					<h1>You are not enrolled in any course.</h1>
				</table>
				`

		} else {

		enrollments.map(course => {

			// console.log(course);

			fetch(`https://calm-ravine-25596.herokuapp.com/api/courses/${course.courseId}`)
			.then(res => res.json())
			.then(specificCourse => {

				// console.log(specificCourse);

				let active = specificCourse.isActive

				// console.log(active)

				cardFooter =
					`
					<a href="./delist.html?courseId=${specificCourse._id}" value="${specificCourse._id}" class="btn btn-danger text-white btn-block deleteButton">Delist</a>
					`

				if (active === true) {

				courses.innerHTML +=

					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body" id="coursePageProfile">
								<h5 class="card-title text-left">
									${specificCourse.name}
								</h5>
								<p class="card-text text-left" id="specificCourseDescription">
									${specificCourse.description}
								</p>
								<p></p>
							</div>
							<div class="card-footer">
								${cardFooter}								
							</div>
						</div>
					</div>
					`
				}

			})

			
		}).join("")

		}

	})

}