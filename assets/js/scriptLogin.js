let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileBtn = document.querySelector("#profileBtn");

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {

	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link text-white"><span id="shortcuts-dark-inactive"> Log In </span> </a>
		</li>
	`
	registerBtn.innerHTML =
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link text-white"><span id="shortcuts-dark"> Register </span></a>
		</li>
	`

} else {

	profileBtn.innerHTML = 	
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	`	

	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	`

}